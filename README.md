# Workstation setup

This is an attempt to start automating the setting up of a new workstation using
[Ansible](http://www.ansible.com/).

## How to run it

```sh
ansible-playbook -i inventory.ini site.yml
```
